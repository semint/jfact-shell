# Run

## requirements

        java 8 runtime


You can [build from source](build.md) or [download a ready artifact](https://bitbucket.org/semint/jfact-shell/downloads/).

## run command

        java -jar target/Artifact.jar

you should see on console something like:

        App jfact-shell version 0.1.0-SNAPSHOT
        Say 'bye' to exit.
        Try 'load pizza'.


## run in debug mode

You can run the shell in debug mode to get full stacktrace:

        java -jar target/Artifact.jar -debug
