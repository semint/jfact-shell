# Notes

## JFact supported services

realisation, classification, satisfiability, entailment, consistency.

## Explain Use Case

https://github.com/matthewhorridge/owlexplanation/blob/master/src/test/java/org/semanticweb/owl/explanation/impl/blackbox/BlackBoxExplanationGenerator2_TestCase.java

## Turtle Parser API

https://bitbucket.org/openrdf/sesame/src/master/core/rio/turtle/src/test/java/org/openrdf/rio/turtle/TestTurtleParser.java

## Dubbi

1. Perchè serve un'istanza per creare una classe?