# References

1. [OWL Manchester](http://owl.man.ac.uk/documentation.shtml)
2. [Reasoning Exmaples](http://owl.man.ac.uk/2005/07/sssw/)
3. [University Example](http://owl.man.ac.uk/2005/07/sssw/university.html)
4. [Working Ontologist Examples](http://workingontologist.org/Examples/)
5. [OWL2 Tutorial](https://dior.ics.muni.cz/~makub/owl/)
6. [OWL Syntaxes](http://ontogenesis.knowledgeblog.org/88)
7. [OWL Explanation](http://owl.cs.manchester.ac.uk/research/explanation/)
8. [RDF Tutorial](http://docs.rdf4j.org/rdf-tutorial/)
9. [SWRL Rules](https://dior.ics.muni.cz/~makub/owl/#swrl)
10. [OWLAPI Create Tutorial](https://github.com/jottinger/owlapi-tutorial/blob/master/src/tutorial/tutorial.md)
11. [OWLAPI Query Tutorial](https://github.com/owlcs/owlapi/wiki/DL-Queries-with-a-real-reasoner)
12. [OWLAPI4 Examples](https://github.com/owlcs/owlapi/blob/version4/contract/src/test/java/org/semanticweb/owlapi/examples/Examples.java)