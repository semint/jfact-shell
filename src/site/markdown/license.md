# License

## Creative Commons Attribution-ShareAlike 4.0 International License

![Create Commons SA Logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
[Text License](http://creativecommons.org/licenses/by-sa/4.0/)
