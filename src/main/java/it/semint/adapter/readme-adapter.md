# Adapter package
Adapters are domain dependent concrete classes with naming convention
 
    adapter.<domain-element>.<technology>
    adapter.employee.file.EmployeeFileReader
    adapter.employee.file.EmployeeRepositoryFile

    apater.<usecase-name>.<technology>

    apater.<service-name>.<technology>
    
    