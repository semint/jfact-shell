package it.semint.adapter.shell;

import it.semint.Application;
import it.semint.adapter.shell.command.Command;
import it.semint.adapter.shell.command.query.PrintIntroHelp;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

import java.util.Collections;

public class ShellApplication {

    public static void main(String[] args) {
        Config config = new Config();
        config.isDebugOn = args.length>0  && "-debug".equals(args[0]);
        config.isProgressOn = args.length>0  && "-progress".equals(args[0]);
        new ShellApplication().run(config);
    }

    /**
     * Runs main shell command parse execute loop.
     * @param config, if .isDebugOn prints stacktraces. if .isProgressOn prints reasoner progress .
     */
    private void run(Config config) {
        printStartInfo(config);
        Context context = buildContext(config);
        printHelp(context);
        loopParseExecuteUntilBye(config.isDebugOn, context, "");
    }

    private void printStartInfo(Config config) {
        printDebugMode(config.isDebugOn);
        printProgressMode(config.isProgressOn);
        printAppVersion();
    }

    private void printAppVersion() {
        Application app = new Application();
        System.out.println(String.format("App %s version %s", app.getAppName(),app.getAppVersion()));
    }

    private void printDebugMode(boolean debugMode) {
        if (debugMode) System.out.println("DEBUG MODE");
        else System.out.println("To run in debug mode append command line arg '-debug'.");
    }

    private void printProgressMode(boolean progressMode) {
        if (progressMode) System.out.println("PROGRESS MODE");
        else System.out.println("To run in progress mode append command line arg '-progress'.");
    }

    private void printHelp(Context context) {
        new PrintIntroHelp().execute(context, Collections.emptyList());
    }

    private void loopParseExecuteUntilBye(boolean debugMode, Context context, String line) {
        do {
            try {
                if (line.length() > 0) {
                    for (Command command: new Parser().parse(line)) command.execute(context,Parser.tokenize(line));
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
                if (debugMode) e.printStackTrace();
            }
            System.out.print(">");
            line = System.console().readLine();
        } while(line!=null && !line.equals("bye"));
    }

    private Context buildContext(Config config) {
        // OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
        OWLReasonerFactory reasonerFactory =  new JFactFactory();
        OWLDataFactory dataFactory = OWLManager.getOWLDataFactory();
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = createOntology(config.isDebugOn, manager);
        OWLReasoner reasoner = reasonerFactory.createReasoner(ontology);;
        return new Context(reasonerFactory, ontology, reasoner, manager,dataFactory, config);
    }

    private OWLOntology createOntology(boolean debugMode, OWLOntologyManager manager) {
        OWLOntology ontology = null;
        try {
            ontology = manager.createOntology();
        } catch (OWLOntologyCreationException e) {
            System.err.println(e.getMessage());
            if (debugMode) e.printStackTrace();
        }
        return ontology;
    }

}
