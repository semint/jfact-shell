package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.reasoner.Node;

import java.util.List;
import java.util.Set;

public class ListUnsatisfiables implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        // get a list of unsatisfiable classes
        Node<OWLClass> bottomNode = context.reasoner.getUnsatisfiableClasses();
        // leave owl:Nothing out
        System.out.println("---------------------");
        Set<OWLClass> unsatisfiable = bottomNode.getEntitiesMinusBottom();
        if (!unsatisfiable.isEmpty()) {
            System.out.println("The following classes are unsatisfiable: ");
            for (OWLClass cls : unsatisfiable) {
                System.out.println(" -- "+cls.getIRI().getFragment());
            }
        } else {
            System.out.println("There are no unsatisfiable classes");
        }
        return Result.OK;
    }
}
