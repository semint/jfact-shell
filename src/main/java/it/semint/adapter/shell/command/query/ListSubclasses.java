package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class ListSubclasses implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (tokens.size() > 1) {
            return new ListSubclassesOfClass().execute(context,tokens);
        }
        return new ListSubclassesAll().execute(context,tokens);
    }
}
