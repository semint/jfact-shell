package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class CheckConsistency implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (context.reasoner.isConsistent()) {
            System.out.println("CONSISTENT!");
        } else {
            System.out.println("NOT CONSISTENT!");
        }
        return Result.OK;
    }
}
