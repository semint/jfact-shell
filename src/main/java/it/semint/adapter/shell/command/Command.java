package it.semint.adapter.shell.command;

import it.semint.adapter.shell.Context;

import java.util.List;

public interface Command {
    enum Result {
        OK, KO
    }
    Result execute(Context context, List<String> tokens);

}
