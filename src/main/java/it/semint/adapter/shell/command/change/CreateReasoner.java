package it.semint.adapter.shell.command.change;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

import java.util.List;

public class CreateReasoner implements Command {

    public static final int TIME_OUT = 50000;

    @Override
    public Result execute(Context context, List<String> tokens) {
        ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
        OWLReasonerConfiguration config;
        if (context.config.isProgressOn)
            config = new SimpleConfiguration(progressMonitor, TIME_OUT);
        else
            config = new SimpleConfiguration(TIME_OUT);
        // Create a reasoner that will reason over our ontology and its imports closure.
        context.reasoner = context.reasonerFactory.createReasoner(context.ontology, config);
        System.out.println("Reasoner created.");
        System.out.println("Reasoner "+context.reasoner.getReasonerName()+" version "+context.reasoner.getReasonerVersion());
        return null;
    }
}
