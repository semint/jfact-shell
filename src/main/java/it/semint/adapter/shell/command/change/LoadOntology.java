package it.semint.adapter.shell.command.change;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.ShellApplication;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.util.List;

public class LoadOntology implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        String ontologyName = tokens.get(1);
        System.out.println("Ontology to load ("+ontologyName+").");
        try {
            context.ontology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(
                    ShellApplication.class.getResourceAsStream("/"+ontologyName+".owl"));
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        System.out.println("Ontology loaded.");
        System.out.println("ClassesInSignature:"+context.ontology.getClassesInSignature().size());
        new CreateReasoner().execute(context, tokens);
        return Result.OK;
    }
}
