package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.reasoner.NodeSet;

import java.util.List;
import java.util.Set;

public class ListInstancesAll implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        // for each class, look up the instances
        System.out.println("---------------------");
        System.out.println(" INSTANCES:");
        Set<OWLNamedIndividual> instances = context.ontology.getIndividualsInSignature();
        for (OWLNamedIndividual i : instances) {
            System.out.println(i.getIRI().getFragment());
            // look up all property assertions
            for (OWLObjectProperty op : context.ontology
                    .getObjectPropertiesInSignature()) {
                NodeSet<OWLNamedIndividual> petValuesNodeSet = context.reasoner
                        .getObjectPropertyValues(i, op);
                for (OWLNamedIndividual value : petValuesNodeSet.getFlattened()) {
                    System.out.println(i.getIRI().getFragment() + "\t"
                            + op.getIRI().getFragment() + "\t"
                            + value.getIRI().getFragment());
                }
            }
        }
        return Result.OK;
    }
}
