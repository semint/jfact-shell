package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class HelpCommand implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        new PrintIntroHelp().execute(context,tokens);
        new PrintOntologyHelp().execute(context,tokens);
        return Result.OK;
    }
}
