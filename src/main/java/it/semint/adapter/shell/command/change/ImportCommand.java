package it.semint.adapter.shell.command.change;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import it.semint.adapter.shell.command.query.PrintOntologyHelp;

import java.util.List;

public class ImportCommand implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        new ImportOntology().execute(context,tokens);
        new PrintOntologyHelp().execute(context,tokens);
        return Result.OK;
    }
}
