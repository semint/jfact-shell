package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import it.semint.adapter.shell.command.CommandHelper;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.reasoner.NodeSet;

import java.util.List;
import java.util.Optional;

public class ListInstancesOfClass implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        Optional<OWLClass> cOpt = CommandHelper.getOWLClass(context, tokens.get(1));
        if (!cOpt.isPresent()) {
            System.err.println("Class not found.");
            return Result.KO;
        }
        OWLClass c = cOpt.get();
        // Look up and print all direct subclasses for all classes
        System.out.println("---------------------");
        System.out.println(" INSTANCES OF CLASS:");
        NodeSet<OWLNamedIndividual> instances = context.reasoner.getInstances(c, false);
        for (OWLNamedIndividual i : instances.getFlattened()) {
            System.out.println(i.getIRI().getFragment() + "\tinstance of\t"
                    + c.getIRI().getFragment());
            // look up all property assertions
            for (OWLObjectProperty op : context.ontology
                    .getObjectPropertiesInSignature()) {
                NodeSet<OWLNamedIndividual> petValuesNodeSet = context.reasoner
                        .getObjectPropertyValues(i, op);
                for (OWLNamedIndividual value : petValuesNodeSet.getFlattened()) {
                    System.out.println(i.getIRI().getFragment() + "\t"
                            + op.getIRI().getFragment() + "\t"
                            + value.getIRI().getFragment());
                }
            }
        }
        return null;
    }
}
