package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.reasoner.NodeSet;

import java.util.List;

public class ListSubclassesAll implements Command {

    @Override
    public Result execute(Context context, List<String> tokens) {
        // Look up and print all direct subclasses for all classes
        System.out.println("---------------------");
        System.out.println(" SUBCLASSES:");
        for (
                OWLClass c : context.ontology.getClassesInSignature()) {
            // the boolean argument specifies direct subclasses; false would
            // specify all subclasses
            // a NodeSet represents a set of Nodes.
            // a Node represents a set of equivalent classes
            NodeSet<OWLClass> subClasses = context.reasoner.getSubClasses(c, true);
            for (OWLClass subClass : subClasses.getFlattened()) {
                System.out.println(subClass.getIRI().getFragment() + "\tsubclass of\t"
                        + c.getIRI().getFragment());
            }
        }
        return Result.OK;
    }
}
