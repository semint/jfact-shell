package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import it.semint.adapter.shell.command.CommandHelper;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.reasoner.NodeSet;

import java.util.List;
import java.util.Optional;

public class ListSubclassesOfClass implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        Optional<OWLClass> c = CommandHelper.getOWLClass(context, tokens.get(1));
        if (!c.isPresent()) {
            System.err.println("Class not found.");
            return Result.KO;
        }
        // Look up and print all direct subclasses for all classes
        System.out.println("---------------------");
        System.out.println(" SUBCLASSES OF CLASS:");
        NodeSet<OWLClass> subClasses = context.reasoner.getSubClasses(c.get(), true);
        for (OWLClass subClass : subClasses.getFlattened()) {
            System.out.println(subClass.getIRI().getFragment() + "\tsubclass of\t"
                    + c.get().getIRI().getFragment());
        }
        return Result.OK;
    }

}
