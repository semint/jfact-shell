package it.semint.adapter.shell.command.change;

import com.google.common.base.Optional;
import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import java.util.List;

public class AddClass implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        String individualName = tokens.get(1);
        String newClassName = tokens.get(2);

        Optional<IRI> defaultDocumentIRI = context.ontology.getOntologyID().getDefaultDocumentIRI();
        if (!defaultDocumentIRI.isPresent()) {
            System.out.println("IRI prefix is required.");
            return Result.KO;
        }
        String defaultPrefix = defaultDocumentIRI.get().toString();
        PrefixManager pm = new DefaultPrefixManager(null, null, defaultPrefix);
        OWLNamedIndividual instance = context.dataFactory.getOWLNamedIndividual(":"+individualName, pm);
        OWLClass newClass = context.dataFactory.getOWLClass(IRI.create(defaultPrefix + "#"+newClassName));
        OWLClassAssertionAxiom classAssertionAxiom  = context.dataFactory.getOWLClassAssertionAxiom(newClass, instance);
        context.manager.addAxiom(context.ontology,classAssertionAxiom);
        return Result.OK;
    }
}
