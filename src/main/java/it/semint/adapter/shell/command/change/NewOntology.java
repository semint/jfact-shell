package it.semint.adapter.shell.command.change;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.util.List;

// import from URL like:
// "https://protege.stanford.edu/ontologies/pizza/pizza.owl"

public class NewOntology implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (tokens.size()!=2) {
            System.out.println("Required a IRI name for the new ontology.");
            return Result.KO;
        }
        String ontologyUrl = tokens.get(1);
        try {
            context.ontology = OWLManager.createOWLOntologyManager().createOntology(IRI.create(ontologyUrl));
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        System.out.println("New ontology created.");
        new CreateReasoner().execute(context, tokens);
        return Result.OK;
    }
}
