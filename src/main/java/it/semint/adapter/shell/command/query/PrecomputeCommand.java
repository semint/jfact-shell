package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class PrecomputeCommand implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (tokens.size()==2 && "hierarchy".equals(tokens.get(1))) {
            return new PrecomputeClassHierarchy().execute(context,tokens);
        }
        return new PrecomputeAll().execute(context,tokens);
    }
}
