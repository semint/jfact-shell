package it.semint.adapter.shell.command;

import it.semint.adapter.shell.Context;
import org.semanticweb.owlapi.model.OWLClass;

import java.util.Optional;

public class CommandHelper {
    static public Optional<OWLClass> getOWLClass(Context context, String name) {
        for (OWLClass c : context.ontology.getClassesInSignature()) {
            if (c.getIRI().getFragment().equals(name)) return Optional.of(c);
        }
        return Optional.empty();
    }
}
