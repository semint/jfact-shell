package it.semint.adapter.shell.command.change;

import com.google.common.base.Optional;
import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import java.util.List;

/*
    FIXME
    WRONG:
###  urn:absolute:GinopolyGino
<urn:absolute:GinopolyGino> rdf:type owl:NamedIndividual ,
                                     :Person .
    RIGHT:
###  http://www.semanticweb.org/nipe/ontologies/2018/11/untitled-ontology-4#Snowball
:Snowball rdf:type owl:NamedIndividual ,
                   :Cat .

 */
public class AddIndividual implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (tokens.size()==1) {
            System.out.println("new Instance name is required.");
            return Result.KO;
        }
        String newIndividualName = tokens.get(1);
        Optional<IRI> defaultDocumentIRI = context.ontology.getOntologyID().getDefaultDocumentIRI();
        if (!defaultDocumentIRI.isPresent()) {
            System.out.println("IRI prefix is required.");
            return Result.KO;
        }
        String defaultPrefix = defaultDocumentIRI.get().toString();
        PrefixManager pm = new DefaultPrefixManager(null, null, defaultPrefix);
        OWLNamedIndividual owlNamedIndividual = context.dataFactory.getOWLNamedIndividual(":"+newIndividualName, pm);
        OWLAxiom axiom = context.dataFactory.getOWLDeclarationAxiom(owlNamedIndividual);
        context.manager.addAxiom(context.ontology,axiom);
        System.out.println("new Instance "+newIndividualName+" added.");
        return Result.OK;
    }
}
