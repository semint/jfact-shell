package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owl.explanation.api.ExplanationGeneratorFactory;
import org.semanticweb.owl.explanation.api.ExplanationManager;
import org.semanticweb.owlapi.model.OWLAxiom;

import java.util.List;
import java.util.Set;

/**
 *  Create the explanation generator factory which uses reasoners provided by the specified reasoner factory.
  */
public class ExplainAxiom implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        ExplanationGeneratorFactory<OWLAxiom> genFac = ExplanationManager.createExplanationGeneratorFactory(context.reasonerFactory);
        // Now create the actual explanation generator for our ontology
        ExplanationGenerator<OWLAxiom> gen = genFac.createExplanationGenerator(context.ontology);
        // Get a reference to the axiom that represents the entailment that we want explanation for
        int axiomPos = Integer.parseInt(tokens.get(1));
        OWLAxiom entailment = (OWLAxiom) context.ontology.getAxioms().toArray()[axiomPos];
        // Ask for explanations for some entailment
        // Get our explanations.  Ask for a maximum of 5.
        Set<Explanation<OWLAxiom>> expl = gen.getExplanations(entailment, 5);
        System.out.println(expl);
        return Result.OK;
    }
}
