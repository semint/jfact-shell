package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.io.StringDocumentTarget;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import java.util.List;

public class ExportTurtle implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        try {
            StringDocumentTarget owlOntologyDocumentTarget = new StringDocumentTarget();
            context.manager.saveOntology(context.ontology, new TurtleDocumentFormat(), owlOntologyDocumentTarget);
            System.out.println(owlOntologyDocumentTarget.toString());
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            return Result.KO;
        }
        return Result.OK;
    }
}
