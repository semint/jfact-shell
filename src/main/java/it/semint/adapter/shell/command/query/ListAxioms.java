package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLAxiom;

import java.util.List;
import java.util.Set;

public class ListAxioms implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        Set<OWLAxiom> axioms = context.ontology.getAxioms();
        boolean onlySubclassOf = tokens.size()> 1 && "SUBCLASSOF".equals(tokens.get(1));
        System.out.println("---------------------");
        System.out.println(" ("+axioms.size()+") AXIOMS :");
        int i=0;
        for (OWLAxiom a: axioms) {
            if (onlySubclassOf) {
                if (a.toString().startsWith("SubClassOf"))
                    System.out.println(String.format("%4d %s", i, a.toString()));
            } else {
                System.out.println(String.format("%4d %s", i++, a.toString()));
            }
        }
        return Result.OK;
    }
}
