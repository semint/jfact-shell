package it.semint.adapter.shell.command.change;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;

import java.util.List;

// TODO AddAxiom
public class AddAxiom implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        System.out.println("TRY Axiom add ");
        //TurtleParser parser = new TurtleParser();

        OWLDataFactory dataFactory = context.manager.getOWLDataFactory();


        // TODO
        //return df.getOWLSubClassOfAxiom(startExpression, superClass);
        // OWLIndividual ind = parseIndividual();





        PrefixOWLOntologyFormat pm = (PrefixOWLOntologyFormat) context.manager.getOntologyFormat(context.ontology).asPrefixOWLOntologyFormat();
//        String BASE_URL = "http://acrab.ics.muni.cz/ontologies/tutorial.owl";
//        pm.setDefaultPrefix(BASE_URL + "#");


        // ManchesterOWLSyntaxParserImpl.parseFrames()
        //Set<OntologyAxiomPair> axioms = new HashSet<>();

        OWLNamedIndividual ivan = dataFactory.getOWLNamedIndividual(":Ivan",pm);
        OWLClass chOMPClass = dataFactory.getOWLClass(":ChildOfMarriedParents", pm);
        OWLClassAssertionAxiom axiomToAdd = dataFactory.getOWLClassAssertionAxiom(chOMPClass, ivan);


        boolean added = context.ontology.getAxioms().add(axiomToAdd);
        System.out.println("Axiom added: "+added);
//        OWLDatatype booleanOWLDatatype = OWLManager.getOWLDataFactory().getBooleanOWLDatatype();
//        OWLEntity entity = OWLManager.getOWLDataFactory().getOWLEntity(booleanOWLDatatype);
//        OWLDeclarationAxiom owlDeclarationAxiom = OWLManager.getOWLDataFactory().getOWLDeclarationAxiom(entity);
//        ontology.getOWLOntologyManager().createOntology().addAxiom(ontology,owlDeclarationAxiom);
        return Result.OK;
    }
}
