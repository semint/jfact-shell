package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import java.util.List;

// FIXME ParseManchester
// FROM https://github.com/owlcs/owlapi/blob/version3/contract/src/test/java/org/coode/owlapi/examples/DLQueryExample.java
/*
>load university1
>NOT Staff
Exception in thread "main" java.lang.NoSuchMethodError: org.semanticweb.owlapi.manchestersyntax.parser.ManchesterOWLSyntaxParserImpl.<init>(Ljavax/inject/Provider;Lorg/semanticweb/owlapi/model/OWLDataFactory;)V
	at org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser.<init>(ManchesterOWLSyntaxEditorParser.java:68)
	at org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser.<init>(ManchesterOWLSyntaxEditorParser.java:54)
	at it.semint.adapter.shell.command.query.DLQueryParser.parseClassExpression(DLQueryParser.java:34)
	at it.semint.adapter.shell.command.query.DLQueryEngine.getSuperClasses(DLQueryEngine.java:28)
	at it.semint.adapter.shell.command.query.DLQueryPrinter.askQuery(DLQueryPrinter.java:25)
	at it.semint.adapter.shell.command.query.ParseManchester.execute(ParseManchester.java:22)
	at it.semint.adapter.shell.ShellApplication.loopParseExecuteUntilBye(ShellApplication.java:66)
	at it.semint.adapter.shell.ShellApplication.run(ShellApplication.java:34)
	at it.semint.adapter.shell.ShellApplication.main(ShellApplication.java:23)


 */
/*
    Manchester OWL:
    C AND D
    C OR D
    NOT C
    {a b c}
    R SOME C
    R ONLY C
    R MIN n
    R MAX n
    R EXACTLY n
    R VALUE a
 */
public class ParseManchester implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        String classExpression = tokens.get(1);
        if (classExpression == null) {
            System.out.println("Manchester classExpression expected.");
            return Result.KO;
        }
        ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
        DLQueryPrinter dlQueryPrinter = new DLQueryPrinter(new DLQueryEngine(context.reasoner,
                shortFormProvider), shortFormProvider);
        dlQueryPrinter.askQuery(classExpression.trim());
        System.out.println();
        return Result.OK;
    }
}
