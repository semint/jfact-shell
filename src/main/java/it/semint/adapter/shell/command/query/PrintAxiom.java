package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLAxiom;

import java.util.List;

public class PrintAxiom implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        if (tokens.size() != 2) {
            System.out.println("syntax: axiom <axiom-nr>");
        }
        String s = (tokens.size()>1) ? tokens.get(1) : "0" ;
        OWLAxiom axiom = (OWLAxiom) context.ontology.getAxioms().toArray()[Integer.parseInt(s)];
        System.out.println(axiom);
        return Result.OK;
    }
}
