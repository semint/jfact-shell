package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.model.OWLClass;

import java.util.List;

public class ListClasses implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        System.out.println("---------------------");
        System.out.println(" CLASSES:");
        for (OWLClass c : context.ontology.getClassesInSignature()) {
            System.out.println(c.getIRI().getFragment());
        }
        return Result.OK;
    }
}
