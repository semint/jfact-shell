package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class PrintIntroHelp implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        System.out.println("Say 'bye' to exit.");
        System.out.println("Say 'help' to get more hints.");
        System.out.println("Try 'load pizza'.");
        System.out.println("Try 'load university1'.");
        System.out.println("Try 'import http://owl.man.ac.uk/2005/07/sssw/teams.owl'.");
        return Result.OK;
    }
}
