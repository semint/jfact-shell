package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;

import java.util.List;

public class PrintOntologyHelp implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        System.out.println("Try 'instances'.");
        System.out.println("Try 'classes'.");
        System.out.println("Try 'subclasses <class-name>'.");
        System.out.println("Try 'compute hierarchy'.");
        System.out.println("Try 'consistency'.");
        System.out.println("Try 'unsatisfiable'.");
        System.out.println("Try 'axioms'.");
        System.out.println("Try 'axioms subclassof'.");
        System.out.println("Try 'axiom <axiom-nr>'.");
        System.out.println("Try 'explain <axiom-nr>'.");
        return Result.OK;
    }
}
