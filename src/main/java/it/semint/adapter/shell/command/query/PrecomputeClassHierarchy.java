package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.reasoner.InferenceType;

import java.util.List;

public class PrecomputeClassHierarchy implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        context.reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
        System.out.println("Hierarchy computed.");
        return Result.OK;
    }
}
