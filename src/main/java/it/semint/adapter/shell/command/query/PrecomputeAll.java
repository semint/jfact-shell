package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.reasoner.InferenceType;

import java.util.List;

public class PrecomputeAll implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        context.reasoner.precomputeInferences(
                InferenceType.CLASS_HIERARCHY,
                InferenceType.CLASS_ASSERTIONS,
                InferenceType.DATA_PROPERTY_ASSERTIONS,
                InferenceType.DATA_PROPERTY_HIERARCHY,
                InferenceType.DISJOINT_CLASSES,
                InferenceType.DIFFERENT_INDIVIDUALS,
                InferenceType.SAME_INDIVIDUAL,
                InferenceType.OBJECT_PROPERTY_ASSERTIONS,
                InferenceType.OBJECT_PROPERTY_HIERARCHY);
        System.out.println("All computed.");
        return Result.OK;
    }
}
