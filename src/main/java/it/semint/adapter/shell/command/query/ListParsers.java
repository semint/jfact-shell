package it.semint.adapter.shell.command.query;

import it.semint.adapter.shell.Context;
import it.semint.adapter.shell.command.Command;
import org.semanticweb.owlapi.apibinding.OWLManager;

import java.util.List;

public class ListParsers implements Command {
    @Override
    public Result execute(Context context, List<String> tokens) {
        OWLManager.createOWLOntologyManager().getOntologyParsers().iterator().forEachRemaining(
                parser -> System.out.println(parser.toString()));
        return Result.OK;
    }
}
