package it.semint.adapter.shell;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class Context {
    public OWLReasonerFactory reasonerFactory;
    public OWLOntology ontology;
    public OWLReasoner reasoner;
    public OWLOntologyManager manager;
    public OWLDataFactory dataFactory;
    public Config config;

    public Context(OWLReasonerFactory reasonerFactory,
                   OWLOntology ontology,
                   OWLReasoner reasoner,
                   OWLOntologyManager manager,
                   OWLDataFactory dataFactory,
                   Config config) {
        this.reasonerFactory = reasonerFactory;
        this.ontology = ontology;
        this.reasoner = reasoner;
        this.manager = manager;
        this.dataFactory = dataFactory;
        this.config = config;
    }
}
