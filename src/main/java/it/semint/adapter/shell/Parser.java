package it.semint.adapter.shell;

import it.semint.adapter.shell.command.*;
import it.semint.adapter.shell.command.change.*;
import it.semint.adapter.shell.command.query.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Convert from string line to list of commands.
 */
class Parser {

    private Map<String,Command> commandMap;

    public Parser() {
        commandMap = new HashMap<>();
        commandMap.put("help",new HelpCommand());
        commandMap.put("new",new NewOntology());
        commandMap.put("load",new LoadOntology());
        commandMap.put("import",new ImportCommand());
        commandMap.put("precompute",new PrecomputeCommand());
        commandMap.put("consistency",new CheckConsistency());
        commandMap.put("unsatisfiable",new ListUnsatisfiables());
        commandMap.put("axiom",new PrintAxiom());
        commandMap.put("axioms",new ListAxioms());
        commandMap.put("instances",new ListInstances());
        commandMap.put("parsers",new ListParsers());
        commandMap.put("explain",new ExplainAxiom());
        commandMap.put("subclasses",new ListSubclasses());
        commandMap.put("classes",new ListClasses());
        commandMap.put("addclass",new AddClass());
        commandMap.put("hierarchy",new PrintHierarchy());
        commandMap.put("add-axiom",new AddAxiom());
        commandMap.put("add-instance",new AddIndividual());
        commandMap.put("add-class",new AddClass());
        commandMap.put("export-turtle",new ExportTurtle());
        commandMap.put("export-manchester",new ExportManchester());
        commandMap.put("export-owl",new ExportOwl());
    }

    List<Command> parse(String line) {
        List<Command> commands = new ArrayList<>();
        List<String> tokens = tokenize(line);
        Command command = buildCommand(tokens);
        command = orListCommands(line, command);
        orTryToParseManchester(commands, command);
        return commands;
    }

    private static void orTryToParseManchester(List<Command> commands, Command command) {
        if (command!=null) commands.add(command);
            else commands.add(new ParseManchester());
    }

    private Command orListCommands(String line, Command command) {
        if ("commands".equals(line)) {
            commandMap.keySet().stream().forEach(System.out::println);
            command = new HelpCommand();
        }
        return command;
    }

    private Command buildCommand(List<String> tokens) {
        return commandMap.get(tokens.get(0));
    }

    static List<String> tokenize(String line) {
        return Collections.list(new StringTokenizer(line)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

}
