# JFACT-SHELL

JFact-shell is an interactive shell to learn [Description Logic](https://en.wikipedia.org/wiki/Description_logic) using [JFact DL reasoner](http://jfact.sourceforge.net/).

1. [License](src/site/markdown/license.md)
2. [Features](src/site/markdown/features.md)
3. [To Do](src/site/markdown/todo.md)
4. [Change Log](src/site/markdown/changelog.md)
5. [Run](src/site/markdown/run.md)
6. [Build](src/site/markdown/build.md)
7. [References](src/site/markdown/references.md)
8. [Notes](src/site/markdown/notes.md)

Authors: [Semantic Integrators](http://www.semint.it)
